var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var babel = require('gulp-babel');
var argv = require('yargs').argv;


gulp.task('live', function() {
    browserSync.init({
        server: "./"
    });

    gulp.watch("style.scss", ['sass']);
    gulp.watch("**/*.html").on('change', browserSync.reload);
    gulp.watch(["main.js", "dev-main.js"], ['babel']).on('change', browserSync.reload);
})

// Static Server + watching scss/html files
gulp.task('dev-serve', ['sass'], function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch("style.scss", ['sass']);
    gulp.watch("**/*.html").on('change', browserSync.reload);
    gulp.watch(["main.js", "dev-main.js"], ['babel']).on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("style.scss")
        .pipe(sass())
        .pipe(gulp.dest("./"))
        .pipe(browserSync.stream());
});

gulp.task('default', ['dev-serve']);

gulp.task('babel', function() {
	return gulp.src(["main.js", "dev-main.js"])
		.pipe(babel({presets: ['react', 'env']
		}))
		.pipe(gulp.dest('./scripts'))
});