<?php

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

if ( defined( 'WP_CLI' ) && WP_CLI ) {
  
  $my_command = function() {

    
    
    $response = WP_CLI::launch_self( 'post list --post_type=page --fields=post_title,ID,post_parent', array(), array( 'format' => 'json' ), false, true );
    
    $pages = json_decode( $response->stdout );
    //var_dump($pages);
    //$pages = array('key0' => array('name', 'id', 'parentId')) + $pages;
    //array_unshift($pages, ["post_title"=>'name', "ID"=>'id',"post_parent"=>'parentId']);

    $tmp_dir = WP_CLI\Utils\get_temp_dir();
    $filepath = $tmp_dir.'/tmpfile.csv';
    $fp = fopen($filepath, 'w');

    foreach ($pages as $fields) {
      fputcsv($fp,get_object_vars($fields));
    }

    fclose($fp);

    echo $filepath."\n";

    $cmd = "cd /Users/robgordon/Dev/web/wp_d3 && wp3d_file=\"".$filepath."\" npm run live";

    WP_CLI::launch( $cmd );


    
    //WP_CLI\Utils\format_items( 'csv', $pages, array( 'post_title', 'ID', 'post_parent' ) );

  };

  WP_CLI::add_command( 'mycommand', $my_command, array(
    // 'before_invoke' => function(){
    //     if ( ! is_multisite() ) {
    //         WP_CLI::error( 'This is not a multisite install.' );
    //     }
    // },
  ) );

}