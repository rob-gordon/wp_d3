'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var routes = [{
	name: 'Home',
	id: '/'
}, {
	name: 'About',
	id: '/about'
}, {
	name: 'Impact',
	id: '/impact'
}, {
	name: 'Research',
	id: '/research'
}, {
	name: 'Projects',
	id: '/projects'
}, {
	name: 'Publications',
	id: '/publications'
}, {
	name: 'News & Events',
	id: '/news-events'
}, {
	name: 'Some News',
	id: '/news-events/some-news-events'
}];

var stratify = d3.stratify().parentId(function (d) {
	if (d.id == "/") {
		return undefined;
	}
	var route_arr = d.id.split("/");
	var parent_id = route_arr.slice(0, route_arr.length - 1).join('/');
	return parent_id ? parent_id : "/";
});

var root = stratify(routes);

var Box = function (_React$Component) {
	_inherits(Box, _React$Component);

	function Box() {
		_classCallCheck(this, Box);

		return _possibleConstructorReturn(this, (Box.__proto__ || Object.getPrototypeOf(Box)).apply(this, arguments));
	}

	_createClass(Box, [{
		key: 'render',
		value: function render() {

			var children;

			if (this.props.item.hasOwnProperty('children')) {
				var actualChildren = this.props.item.children.map(function (child) {
					return React.createElement(Box, { item: child });
				});
				children = React.createElement(
					'div',
					{ className: 'children' },
					actualChildren
				);
			} else {
				children = '';
			}

			return React.createElement(
				'div',
				{ className: 'box' },
				React.createElement(
					'div',
					{ className: 'inner' },
					React.createElement(
						'span',
						{ className: 'name' },
						this.props.item.data.name
					),
					React.createElement(
						'span',
						{ className: 'route' },
						this.props.item.data.id
					)
				),
				children
			);
		}
	}]);

	return Box;
}(React.Component);

var App = function (_React$Component2) {
	_inherits(App, _React$Component2);

	function App(props) {
		_classCallCheck(this, App);

		//this.state = {isToggleOn: true};

		// This binding is necessary to make `this` work in the callback
		var _this2 = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

		_this2.addRoute = _this2.addRoute.bind(_this2);
		return _this2;
	}

	_createClass(App, [{
		key: 'addRoute',
		value: function addRoute() {
			console.log(document.getElementById('newRoute_input').value);
		}
	}, {
		key: 'render',
		value: function render() {

			var listItems;

			if (this.props.routes.constructor == Array) {
				listItems = this.props.routes.map(function (route) {
					return (
						// <Box item={route}/>
						React.createElement(
							'div',
							null,
							'a list appears'
						)
					);
				});
			} else {
				listItems = React.createElement(Box, { item: this.props.routes });
			}

			return React.createElement(
				'div',
				{ className: 'app' },
				listItems,
				React.createElement('br', null),
				React.createElement(
					'label',
					{ htmlFor: 'newRoute' },
					'Add New Route: ',
					React.createElement(
						'strong',
						null,
						'/'
					)
				),
				React.createElement('input', { type: 'text', name: 'newRoute', id: 'newRoute_input' }),
				React.createElement(
					'button',
					{ onClick: this.addRoute },
					'+'
				)
			);
		}
	}]);

	return App;
}(React.Component);

ReactDOM.render(React.createElement(App, { routes: root }), document.getElementById('root'));